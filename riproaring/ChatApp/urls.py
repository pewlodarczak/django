from django.urls import path
from . import views

urlpatterns = [
    path("", views.chatPage, name="chat-page"),
    # path("auth/login/", LoginView.as_view
    #      (template_name="chat/LoginPage.html"), name="login-user"),
    # path("auth/logout/", LogoutView.as_view(), name="logout-user"),
]