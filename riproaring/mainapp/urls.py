from django.urls import path
from . import views  #importing our view file 
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    # path('', index, name='index'),
    path("", views.homepage, name="home"),
]

urlpatterns += staticfiles_urlpatterns()