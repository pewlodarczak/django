# Prerequisites

Python Version 3.x, Django Version 4.x

# Installation

Die Python und pip Version überprüfen

```batch
python --version
pip --version
```
Python muss mindestens Version 3.x sein. <br>
Ein virtuelles Envorinment erstellen und aktivieren:

```batch
mkdir django
cd django
python -m venv .venv
.venv\scripts\activate
```

Den PostgreSQL Treiber und Django Modul installieren:

```batch
pip install psycopg2 django
```

Ein neues Django Projekt und die drei Apps erstellen:

```batch
django-admin startproject riproaring

cd riproaring

python manage.py startapp mainapp
python manage.py startapp blog
python manage.py startapp accounts
```
Admin User erstellen

```batch
python manage.py createsuperuser
```

Das Repository auschecken:

```batch
cd ..

git clone https://gitlab.com/pewlodarczak/django.git temp

xcopy temp\riproaring riproaring /E /H /C /I

cd riproaring
```
Die Datenbankkonfiguration im `riproaring/settings.py` File anpassen:

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'blogs', 
        'USER': 'postgres',
        'PASSWORD': 'xxxxxxxxxx',
        'HOST': '127.0.0.1', 
        'PORT': '5433',
    }
}
```
Die Datenbank `blogs` muss bereits existieren.
Die Migrations ausführen:

```batch
python manage.py migrate
python manage.py makemigrations
python manage.py migrate
```

Den Development Server starten:

```batch
python manage.py runserver
```
Den Link aufrufen:

http://localhost:8000/
