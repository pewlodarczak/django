import psycopg2

# Connect to your postgres DB
conn = psycopg2.connect("dbname=dbtest user=postgres port=5433 password=admin")

# Open a cursor to perform database operations
cur = conn.cursor()

# Execute a query
cur.execute("SELECT * FROM testdb_teacher")

# Retrieve query results
records = cur.fetchall()