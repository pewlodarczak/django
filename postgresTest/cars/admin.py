from django.contrib import admin

from cars.models import Driver, Car, User

admin.site.register(Driver)
admin.site.register(Car)
admin.site.register(User)
